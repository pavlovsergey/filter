import React from 'react';
import Select from 'react-select';
import { useSelector, useDispatch } from 'react-redux';
import { setTermsValue } from 'store/filter/actions/terms';
import { getUrl } from 'components/Filter/utils';
import history from 'routes/history';
import styles from './TermsSelect.module.scss';

const TermsSelect = ({ url }) => {
  const dispatch = useDispatch();

  const data = useSelector((state) =>
    state.filter.terms.data.map((item) => {
      return { ...item, value: item };
    })
  );
  const value = useSelector((state) => state.filter.terms.value);
  const { isLoad } = useSelector((state) => state.filter.terms.communication);

  const handleChange = (e) => {
    dispatch(setTermsValue(e));
    url.s = e ? `/s-${e.slug}` : null;
    history.push(getUrl(url));
  };

  return (
    <div className={styles.container}>
      <div className={styles.title}> Terms </div>
      <Select
        value={value}
        isDisabled={!isLoad}
        isClearable={true}
        options={data}
        onChange={handleChange}
      />
    </div>
  );
};

export default TermsSelect;
