import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import BrandsSelect from './BrandsSelect';
import TermsSelect from './TermsSelect';
import StylesSelect from './StylesSelect';
import { getFilterData } from 'store/filter/actions';
import styles from './Filter.module.scss';

const Filter = () => {
  const { isLoading, isLoad, isErrorLoad } = useSelector(
    (state) => state.filter.communication
  );

  const url = useSelector((state) => {
    return {
      s: state.filter.terms.value
        ? `/s-${state.filter.terms.value.slug}`
        : null,
      b: state.filter.brands.value
        ? `/b-${state.filter.brands.value.slug}`
        : null,

      st: state.filter.styles.value
        ? `/st-${state.filter.styles.value.slug}`
        : null,
    };
  });

  const dispatch = useDispatch();

  useEffect(() => {
    if (!isLoading && !isLoad && !isErrorLoad) {
      dispatch(getFilterData());
    }
  }, [dispatch, isLoading, isLoad, isErrorLoad]);

  return (
    <div className={styles.container}>
      <div className={styles.selectContainer}>
        <TermsSelect url={url} />
      </div>

      <div className={styles.selectContainer}>
        <BrandsSelect url={url} />
      </div>

      <div className={styles.selectContainer}>
        <StylesSelect url={url} />
      </div>
    </div>
  );
};

export default Filter;
