import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Select from 'react-select';
import { setBrandsValue } from 'store/filter/actions/brands';
import { getUrl } from 'components/Filter/utils';
import history from 'routes/history';
import styles from './BrandsSelect.module.scss';

const BrandsSelect = ({ url }) => {
  const dispatch = useDispatch();

  const data = useSelector((state) =>
    state.filter.brands.data.map((item) => {
      return { ...item, value: item };
    })
  );

  const value = useSelector((state) => state.filter.brands.value);

  const { isLoad } = useSelector((state) => state.filter.brands.communication);

  const handleChange = (e) => {
    dispatch(setBrandsValue(e));
    url.b = e ? `/b-${e.slug}` : null;
    history.push(getUrl(url));
  };

  return (
    <div className={styles.container}>
      <div className={styles.title}> Brands </div>
      <Select
        value={value}
        isClearable={true}
        options={data}
        onChange={handleChange}
        isDisabled={!isLoad}
      />
    </div>
  );
};

export default BrandsSelect;
