import React from 'react';
import Select from 'react-select';
import { useSelector, useDispatch } from 'react-redux';
import { setStylesValue } from 'store/filter/actions/styles';
import { getUrl } from 'components/Filter/utils';
import history from 'routes/history';
import styles from './StylesSelect.module.scss';

const StylesSelect = ({ url }) => {
  const dispatch = useDispatch();

  const data = useSelector((state) =>
    state.filter.styles.data.map((item) => {
      return { ...item, value: item };
    })
  );

  const value = useSelector((state) => state.filter.styles.value);
  const { isLoad } = useSelector((state) => state.filter.styles.communication);

  const handleChange = (e) => {
    dispatch(setStylesValue(e));
    url.st = e ? `/st-${e.slug}` : null;
    history.push(getUrl(url));
  };
  return (
    <div className={styles.container}>
      <div className={styles.title}> Styles </div>
      <Select
        value={value}
        isDisabled={!isLoad}
        isClearable={true}
        options={data}
        onChange={handleChange}
      />
    </div>
  );
};

export default StylesSelect;
