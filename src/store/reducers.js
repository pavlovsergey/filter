import { combineReducers } from 'redux';
import filter from 'store/filter/reducer';

export default combineReducers({ filter });
