export const GET_FILTER = 'GET_FILTER';
export const GET_FILTER_TERMS = 'GET_FILTER_TERMS';
export const SET_TERMS_VALUE = 'SET_TERMS_VALUE';
export const GET_FILTER_BRANDS = 'GET_FILTER_BRANDS';
export const SET_BRANDS_VALUE = 'SET_BRANDS_VALUE';
export const GET_FILTER_STYLES = 'GET_FILTER_STYLES';
export const SET_STYLES_VALUE = 'SET_STYLES_VALUE';
