import { GET_FILTER_STYLES, GET_FILTER } from 'store/filter/constants';

const stateDefault = [];

const data = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case GET_FILTER_STYLES:
      return payload.data;

    case GET_FILTER:
      return payload.style ? [payload.style] : state;

    default:
      return state;
  }
};

export default data;
