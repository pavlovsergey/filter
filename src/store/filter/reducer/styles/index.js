import { combineReducers } from 'redux';
import communication from './communication';
import data from './data';
import value from './value';

export default combineReducers({ communication, data, value });
