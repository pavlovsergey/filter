import { SET_STYLES_VALUE, GET_FILTER } from 'store/filter/constants';

const stateDefault = null;

const value = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case SET_STYLES_VALUE:
      return payload.value;

    case GET_FILTER:
      return payload.style ? payload.style : null;

    default:
      return state;
  }
};

export default value;
