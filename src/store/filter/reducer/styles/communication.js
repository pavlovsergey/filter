import { GET_FILTER_STYLES } from 'store/filter/constants';

import { loadingAction, errAction } from 'utils/store/communication';

const stateDefault = {
  isLoading: false,
  isLoad: false,
  isErrorLoad: false,
  error: { message: '', name: '' },
};

const communication = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case loadingAction(GET_FILTER_STYLES):
      return {
        ...state,
        isLoading: true,
        isLoad: false,
        isErrorLoad: false,
        error: { message: '', name: '' },
      };

    case GET_FILTER_STYLES:
      return {
        ...state,
        isLoading: false,
        isLoad: true,
      };

    case errAction(GET_FILTER_STYLES):
      return {
        ...state,
        isLoading: false,
        isErrorLoad: true,
        error: { message: payload.message },
      };

    default:
      return state;
  }
};

export default communication;
