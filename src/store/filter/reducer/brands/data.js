import { GET_FILTER_BRANDS, GET_FILTER } from 'store/filter/constants';

const stateDefault = [];

const data = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case GET_FILTER_BRANDS:
      return payload.data;

    case GET_FILTER:
      return payload.brand ? [payload.brand] : state;

    default:
      return state;
  }
};

export default data;
