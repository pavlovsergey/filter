import { SET_BRANDS_VALUE, GET_FILTER } from 'store/filter/constants';

const stateDefault = null;

const value = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case SET_BRANDS_VALUE:
      return payload.value;

    case GET_FILTER:
      return payload.brand;

    default:
      return state;
  }
};

export default value;
