import { combineReducers } from 'redux';
import terms from './terms';
import brands from './brands';
import styles from './styles';
import communication from './communication';

export default combineReducers({ terms, brands, styles, communication });
