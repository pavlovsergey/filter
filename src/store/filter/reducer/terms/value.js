import { SET_TERMS_VALUE, GET_FILTER } from 'store/filter/constants';

const stateDefault = null;

const value = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case SET_TERMS_VALUE:
      return payload.value;

    case GET_FILTER:
      return payload.service ? payload.service : null;

    default:
      return state;
  }
};

export default value;
