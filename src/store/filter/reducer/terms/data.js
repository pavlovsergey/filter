import { GET_FILTER_TERMS, GET_FILTER } from 'store/filter/constants';

const stateDefault = [];

const data = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case GET_FILTER_TERMS:
      return payload.data;

    case GET_FILTER:
      return payload.service ? [payload.service] : state;

    default:
      return state;
  }
};

export default data;
