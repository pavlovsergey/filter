import api from 'utils/api';
import { GET_FILTER_STYLES, SET_STYLES_VALUE } from 'store/filter/constants';
import { loadingAction, errAction } from 'utils/store/communication';

export const getFilterStyles = () => async (dispatch) => {
  dispatch({ type: loadingAction(GET_FILTER_STYLES) });

  try {
    const { data } = await api.getFilterStyles();
    dispatch({ type: GET_FILTER_STYLES, payload: data });
  } catch (e) {
    dispatch({
      type: errAction(GET_FILTER_STYLES),
      payload: { message: e.message },
    });
  }
};

export const setStylesValue = (value) => {
  return { type: SET_STYLES_VALUE, payload: { value } };
};
