import { GET_FILTER } from 'store/filter/constants';
import { loadingAction, errAction } from 'utils/store/communication';
import { getFilterParams } from './utils';
import { getFilterBrands } from 'store/filter/actions/brands';
import { getFilterStyles } from 'store/filter/actions/styles';
import { getFilterTerms } from 'store/filter/actions/terms';

import api from 'utils/api';

export const getFilterData = () => async (dispatch) => {
  dispatch({ type: loadingAction(GET_FILTER) });

  try {
    const { data } = await api.getFilter(getFilterParams());
    data.brand = Object.values(data.brand).length ? data.brand : null;
    data.style = Object.values(data.style).length ? data.style : null;
    data.service = Object.values(data.service).length ? data.service : null;

    dispatch({ type: GET_FILTER, payload: data });
    dispatch(getFilterBrands());
    dispatch(getFilterStyles());
    dispatch(getFilterTerms());
  } catch (e) {
    dispatch({
      type: errAction(GET_FILTER),
      payload: { message: e.message },
    });
  }
};
