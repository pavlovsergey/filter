import api from 'utils/api';
import { GET_FILTER_TERMS, SET_TERMS_VALUE } from 'store/filter/constants';
import { loadingAction, errAction } from 'utils/store/communication';

export const getFilterTerms = () => async (dispatch) => {
  dispatch({ type: loadingAction(GET_FILTER_TERMS) });

  try {
    const { data } = await api.getFilterTerms();
    dispatch({ type: GET_FILTER_TERMS, payload: data });
  } catch (e) {
    dispatch({
      type: errAction(GET_FILTER_TERMS),
      payload: { message: e.message },
    });
  }
};

export const setTermsValue = (value) => {
  return { type: SET_TERMS_VALUE, payload: { value } };
};
