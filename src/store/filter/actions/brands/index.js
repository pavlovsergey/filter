import api from 'utils/api';
import { GET_FILTER_BRANDS, SET_BRANDS_VALUE } from 'store/filter/constants';
import { loadingAction, errAction } from 'utils/store/communication';

export const getFilterBrands = () => async (dispatch) => {
  dispatch({ type: loadingAction(GET_FILTER_BRANDS) });

  try {
    const { data } = await api.getFilterBrands();
    dispatch({ type: GET_FILTER_BRANDS, payload: data });
  } catch (e) {
    dispatch({
      type: errAction(GET_FILTER_BRANDS),
      payload: { message: e.message },
    });
  }
};

export const setBrandsValue = (value) => {
  return { type: SET_BRANDS_VALUE, payload: { value } };
};
