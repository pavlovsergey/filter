export const getFilterParams = () => {
  const href = window.location.href;
  const service_slug = href.split('/s-')[1]
    ? href.split('/s-')[1].split('/')[0]
    : null;

  const brand_slug = href.split('/b-')[1]
    ? href.split('/b-')[1].split('/')[0]
    : null;

  const style_slug = href.split('/st-')[1]
    ? href.split('/st-')[1].split('/')[0]
    : null;

  return { service_slug, brand_slug, style_slug };
};
