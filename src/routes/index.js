import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Dashboard from 'pages/Dashboard';

const RouterApp = () => {
  return (
    <Switch>
      <Route component={Dashboard} />
    </Switch>
  );
};

export default RouterApp;
