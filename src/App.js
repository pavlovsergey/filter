import React from 'react';
import RouterApp from './routes';
import { Provider } from 'react-redux';
import store from './store';
import { Router } from 'react-router-dom';
import history from './routes/history';
import styles from './App.module.scss';

const App = () => {
  return (
    <Provider store={store}>
      <div className={styles.layout}>
        <Router history={history}>
          <RouterApp />
        </Router>
      </div>
    </Provider>
  );
};

export default App;
