import React from 'react';
import Filter from 'components/Filter';
import styles from './Dashboard.module.scss';

const Dashboard = () => {
  return (
    <div className={styles.container}>
      <Filter />
    </div>
  );
};

export default Dashboard;
