import axios from 'axios';

axios.defaults.baseURL = 'https://beta.autobooking.com/api/test/v1';

export const getFilterTerms = () => {
  return axios.get(`/search/terms?timestamp=${new Date().getTime()}`);
};

export const getFilterBrands = () => {
  return axios.get(`/search/brands_terms?timestamp=${new Date().getTime()}`);
};

export const getFilterStyles = () => {
  return axios.get(`/search/styles?timestamp=${new Date().getTime()}`);
};

export const getFilter = ({ service_slug, brand_slug, style_slug }) => {
  return axios.get(`/search/parse_link`, {
    params: {
      service_slug,
      brand_slug,
      style_slug,
      timestamp: new Date().getTime(),
    },
  });
};
