export const loadingAction = (action) => {
  return `${action}_LOADING`;
};

export const errAction = (action) => {
  return `${action}_FAIL`;
};

export const clearAction = (action) => {
  return `${action}_CLEAR`;
};
